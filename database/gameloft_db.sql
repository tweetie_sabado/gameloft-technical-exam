-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 20, 2016 at 11:32 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gameloft_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE IF NOT EXISTS `subscribers` (
`subId` int(11) NOT NULL,
  `firstName` varchar(25) NOT NULL,
  `lastName` varchar(25) NOT NULL,
  `subType` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`subId`, `firstName`, `lastName`, `subType`) VALUES
(1, 'Tweetie', 'Sabado', 'Premium'),
(3, 'Mariel', 'Hicaro', 'Premium'),
(4, 'Maricel', 'Soriano', 'Premium'),
(5, 'Bea', 'Alonzo', 'Premium'),
(6, 'Angel', 'Locsin', 'Premium'),
(8, 'Amy ', 'Adams', 'Premium'),
(9, 'Jessy', 'Mendiola', 'Premium'),
(10, 'Dany', 'Targaryen', 'Premium'),
(11, 'Tyrion', 'Lannister', 'Premium'),
(21, 'Cersei ', 'Lannister', 'Premium'),
(22, 'Jeoffrey', 'Barratheon', 'Premium'),
(23, 'Tommen', 'Barratheon', 'Premium'),
(24, 'Sansa', 'Stark', 'Premium'),
(25, 'Jon', 'Snow', 'Regular');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`userId` int(11) NOT NULL,
  `emailAdd` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `emailAdd`, `password`) VALUES
(1, 'hello@example.com', 'testing'),
(2, 'hayley@gmail.com', 'iamhappy'),
(3, 'meghan@trainor.com', 'metoo'),
(4, 'huhu@email.com', 'killemkind');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
 ADD PRIMARY KEY (`subId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
MODIFY `subId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
