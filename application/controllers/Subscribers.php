<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscribers extends CI_Controller {
  public function __construct()
	{
		parent::__construct();
		$this->load->model('subscribers_model','subscriber');
	}

  public function index()
	{
    $data['accountName'] = $this->session ->userdata('accountName');
    $data['content'] = 'admin';
    $this->load->view('includes/template', $data);
	}

  public function list_subscribers()
	{
		$list = $this->subscriber->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $subscriber) {
			$no++;
			$row = array();
			$row[] = $subscriber->firstName;
			$row[] = $subscriber->lastName;
			$row[] = $subscriber->subType;

			//add html for action
			$row[] = '<a class="waves-effect waves-light btn lime" href="javascript:void(0)" title="Edit" onclick="edit_sub('."'".$subscriber->subId."'".')">Edit</a>
				        <a class="waves-effect waves-light btn red" href="javascript:void(0)" title="Delete" onclick="delete_sub('."'".$subscriber->subId."'".')">Delete</a>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->subscriber->count_all(),
			"recordsFiltered" => $this->subscriber->count_filtered(),
			"data" => $data,
		);

		echo json_encode($output);
	}

  //CREATE
  public function add_subscriber()
	{
    $this->_validate();
		$data = array(
      				'firstName' => $this->input->post('firstName'),
      				'lastName' => $this->input->post('lastName'),
      				'subType' => $this->input->post('subType')
            );
		$insert = $this->subscriber->save($data);
		echo json_encode(array("status" => TRUE));
	}

  //READ
  public function get_subscriber($id)
  {
    $data = $this->subscriber->get_by_id($id);
    echo json_encode($data);
  }

  //UPDATE
  public function update_subscriber()
	{
		$this->_validate();
		$data = array(
              'firstName' => $this->input->post('firstName'),
              'lastName' => $this->input->post('lastName'),
              'subType' => $this->input->post('subType')
			      );
		$this->subscriber->update(array('subId' => $this->input->post('subId')), $data);
		echo json_encode(array("status" => TRUE));
	}

  //DELETE
  public function delete_subscriber($id)
	{
		$this->subscriber->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

  private function _validate()
  {
    $data = array();
    $data['error_string'] = array();
    $data['inputerror'] = array();
    $data['status'] = TRUE;

    if($this->input->post('firstName') == '')
    {
      $data['inputerror'][] = 'firstName';
      $data['error_string'][] = 'First name is required';
      $data['status'] = FALSE;
    }

    if($this->input->post('lastName') == '')
    {
      $data['inputerror'][] = 'lastName';
      $data['error_string'][] = 'Last name is required';
      $data['status'] = FALSE;
    }

    if($this->input->post('subType') == '')
    {
      $data['inputerror'][] = 'subType';
      $data['error_string'][] = 'Please choose a subscription type';
      $data['status'] = FALSE;
    }

    if($data['status'] === FALSE)
    {
      echo json_encode($data);
      exit();
    }
  }

}
