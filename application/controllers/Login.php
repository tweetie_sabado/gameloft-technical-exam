<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('login_model','login');
	}

	public function index()
	{
    $data['content'] = 'login';
		$this->load->view('includes/template', $data);
	}

  function validate_account()
  {
    $this->form_validation->set_rules('email', 'Username', 'required|callback_check_account');
		$this->form_validation->set_rules('password', 'Password','required');
    if($this->form_validation->run() == false){
         redirect('login');
    }
    else{
         redirect('administrator');
    }
   }

   function check_account()
   {
			$email = $this->input->post('email');
			$pass = $this->input->post('password');

      $query = $this->login->check_account_db($email,$pass);
      if($query){
      	return true;
      }
      else{
        return false;
      }
   }

}
