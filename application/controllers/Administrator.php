<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrator extends CI_Controller {
  public function __construct()
  {
    parent::__construct();
   	$this->is_logged_in();
  }

  public function index()
	{
    $data['accountName'] = $this->session->userdata('email');
    $data['content'] = 'admin';
		$this->load->view('includes/template', $data);
	}

  function is_logged_in()
	{
			$is_logged_in = $this->session->userdata('is_logged_in');

			if(!isset($is_logged_in) || $is_logged_in != true)
			{
					redirect('login');
			}
			else{
					return true;
			}
	}

  function logout(){
    $this->session->sess_destroy();
		 redirect('login');
  }
}
