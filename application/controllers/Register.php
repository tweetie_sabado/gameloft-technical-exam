<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('register_model', 'register');
	}

	public function index()
	{
    $data['content'] = 'register';
		$this->load->view('includes/template', $data);
	}

	function add_user()
	{
		$this->register->add_user();
		$this->session->set_flashdata('success', 'New user successfully created!');
		redirect('login');
	}
}
