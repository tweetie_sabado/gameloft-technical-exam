<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model{

  function check_account_db($email,$pass)
  {
    $this->db->where('emailAdd', $email);
    $this->db->where('password',$pass);
    $query = $this->db->get('users');

    if($query->num_rows() == 1)
    {
      $row = $query->row();
      $data = array(
        'email' => $row->emailAdd,
        'is_logged_in' => true
      );
      $this->session->set_userdata($data);
      return true;
    }
    else{
      $this->session->set_flashdata('error', 'Invalid email/password');
      return false;
    }
  }


}
