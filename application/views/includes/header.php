<!DOCTYPE html>
<html lang="en">

<!--================================================================================
    T J S A B A D O
================================================================================ -->

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="Gameloft Technical Exam">
  <!-- <meta name="keywords" content=""> -->
  <title>Gameloft Technical Exam</title>

  <!-- Favicons-->
  <link rel="icon" href="<?php echo base_url('images/favicon/favicon-32x32.ico');?>" sizes="32x32">

  <!-- CORE CSS-->
  <link href="<?php echo base_url('materialize/css/materialize.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- CUSTOM CSS -->
  <link href="<?php echo base_url('css/style.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Data Tables -->
  <link href="<?php echo base_url('materialize/js/plugins/data-tables/css/jquery.dataTables.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- jQuery Library -->
  <script type="text/javascript" src="<?php echo base_url('materialize/js/plugins/jquery-2.1.1.min.js');?>"></script>
  <!-- jQuery Validate Library -->
  <script type="text/javascript" src="<?php echo base_url('materialize/js/plugins/jquery.validate.1.15.0.js');?>"></script>
</head>
