  <!-- ================================================
    Scripts
    ================================================ -->

  <!-- jQuery Library -->
  <script type="text/javascript" src="<?php echo base_url('materialize/js/plugins/jquery-2.1.1.min.js');?>"></script>
  <!-- Data Tables -->
  <script type="text/javascript" src="<?php echo base_url('materialize/js/plugins/data-tables/js/jquery.dataTables.min.js');?>"></script>
  <!--materialize js-->
  <script type="text/javascript" src="<?php echo base_url('materialize/js/materialize.min.js');?>"></script>
</body>
</html>
