<nav>
  <div class="nav-wrapper">
    <a href="#!" class="brand-logo">Welcome, <?php echo $accountName;?>!</a>
    <ul class="right hide-on-med-and-down">
      <li><a href="<?php echo site_url('administrator/logout');?>">Logout</a></li>
    </ul>
  </div>
</nav>
<body>
    <div class="container">
        <h3>Basic CRUD Functions</h3>
        <br />
          <button class="waves-effect waves-light btn green" onclick="add_person()">Add Subscriber</button>
          <button class="waves-effect waves-light btn green" onclick="reload()">Reload</button>
        <br />
        <br />
        <table id="table" class="responsive-table bordered" cellspacing="0">
            <thead>
              <tr>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Subscription Type</th>
                  <th class="right-align">Action</th>
              </tr>
            </thead>
            <tfoot>
               <tr>
                   <th>First Name</th>
                   <th>Last Name</th>
                   <th>Subscription Type</th>
                   <th class="right-align">Action</th>
               </tr>
            </tfoot>
            <tbody>
            </tbody>
        </table>
        <br />
        <br />
    </div>

<script type="text/javascript">

var method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({

        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?php echo site_url('subscribers/list_subscribers')?>",
            "type": "POST"
        },
        "columnDefs": [
          {
              "targets": [ -1 ],
              "orderable": false,
              "class": "right-align"
          },
        ]
    });

    //material-select
    $('select').material_select();

});

//FUNCTIONS
function add_person()
{
    method = 'add';
    $('#form')[0].reset();
    $('div').removeClass('error');
    $('.error-msg').empty();
    $('#modalForm').openModal();
    $('#modalTitle').text('Add Subscriber');
}

function edit_sub(id)
{
    method = 'update';
    $('#form')[0].reset();
    $('div').removeClass('error');
    $('.error-msg').empty();

    //viewing purposes
    $.ajax({
        url : "<?php echo site_url('subscribers/get_subscriber/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="subId"]').val(data.subId);
            $('[name="firstName"]').val(data.firstName);
            $('[name="lastName"]').val(data.lastName);
            $('[name="subType"]').val(data.subType);
            $('#modalForm').openModal();
            $('#modalTitle').text('Edit Subscriber');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('An error occured while retrieving data');
        }
    });
}


function reload()
{
    table.ajax.reload(null,false);
}

function save()
{
    $('#btnSave').text('saving...');
    $('#btnSave').attr('disabled',true);
    var url;

    if(method == 'add') {
        url = "<?php echo site_url('subscribers/add_subscriber')?>";
    } else {
        url = "<?php echo site_url('subscribers/update_subscriber')?>";
    }

    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if(data.status)
            {
                $('#modalForm').closeModal();
                reload();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++)
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().append( "<div class='error error-msg'></div>" );
                    $('[name="'+data.inputerror[i]+'"]').next().next().text(data.error_string[i]);
                    //$('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                }
            }
            $('#btnSave').text('save');
            $('#btnSave').attr('disabled',false);


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('save');
            $('#btnSave').attr('disabled',false);

        }
    });
}

function delete_sub(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('subscribers/delete_subscriber')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                $('#modalForm').closeModal();
                reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('An error occured while deleting data');
            }
        });

    }
}
</script>

<!-- MODAL -->
<div class="modal" id="modalForm" role="dialog">
  <div class="modal-content">
      <h4 id="modalTitle" class="center-align">Form</h4>
      <div class="divider"></div>
      <br>
      <form action="#" id="form" class="form-horizontal">
        <input type="hidden" value="" name="subId"/>
        <div class="row">
          <div class="input-field col s12">
            <input placeholder="" id="firstName" name="firstName" type="text">
            <label for="first-name">First Name</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input placeholder="" id="lastName" name="lastName" type="text">
            <label for="last-name">Last Name</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
             <select id="subType" name="subType">
               <option value="" disabled selected>Choose your option</option>
               <option value="Regular">Regular</option>
               <option value="Premium">Premium</option>
             </select>
             <label>Subscription Type</label>
           </div>
        </div>
      </form>
      <div class="row right-align">
          <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
          <button type="button" class="waves-effect waves-light btn grey modal-close">Cancel</button>
      </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal -->
<!-- END OF MODAL -->
