<body class="grey lighten-3">
  <div class="section"></div>
  <main>
    <?php if($this->session->flashdata('success')){?>
    <script type="text/javascript">
      $(document).ready(function(){
       var $toastContent = $('<span><?php echo $this->session->flashdata('success');?></span>');
       Materialize.toast($toastContent, 5000)
      });
    </script>
    <?php }?>
    <?php if($this->session->flashdata('error')){?>
    <script type="text/javascript">
      $(document).ready(function(){
       var $toastContent = $('<span><?php echo $this->session->flashdata('error');?></span>');
       Materialize.toast($toastContent, 5000)
      });
    </script>
    <?php }?>
    <center>
      <div class="section"></div>

      <h5 class="grey-darken-4-text">Register</h5>
      <div class="section"></div>

      <div class="container">
        <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">
          <?php $att = array(
                          "class" => "col s12",
                          "id" => "registerForm",
                          "novalidate" => "novalidate"
                      );
                echo form_open('register/add_user', $att);?>
            <div class='row'>
              <div class='col s12'>
              </div>
            </div>

            <div class='row'>
              <div class='input-field col s12'>
                <input type='email' name='email' id='email' />
                <label for='email'>Email</label>
              </div>
            </div>

            <div class='row'>
              <div class='input-field col s12'>
                <input type='password' name='password' id='password' />
                <label for='password'>Password</label>
              </div>
            </div>

            <div class='row'>
              <div class='input-field col s12'>
                <input type='password' name='conf_password' id='conf_password' />
                <label for='conf_password'>Confirm Password</label>
              </div>
              <label style='float: right;'>
                <br /><br />
								<a href='<?php echo site_url();?>login'><b>Go back to Login</b></a>
							</label>
            </div>
            <br />
            <center>
              <div class='row'>
                <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect grey darken-3'>Register</button>
              </div>
            </center>
          </form>
        </div>
      </div>
    </center>
  </main>

<script>
  $("#registerForm").validate({
         rules: {
             email: {
                 required: true,
                 email:true
             },
             password: {
                 required: true,
                 minlength: 5
             },
             conf_password: {
                 required: true,
                 minlength: 5,
                 equalTo: "#password"
             }
        },
         //For custom messages
         messages: {
             email:{
                 required: "Enter your email address",
             },
             password: {
                required: "Enter your password",
                minlength: "Enter at least 5 characters"
             },
             conf_password: {
                required: "Please confirm your password",
                minlength: "Enter at least 5 characters",
                equalTo: "Your passwords do not match"
             }
         },
         errorElement : 'div',
         errorPlacement: function(error, element) {
           var placement = $(element).data('error');
           if (placement) {
             $(placement).append(error)
           } else {
             error.insertAfter(element);
           }
         }
      });
</script>
